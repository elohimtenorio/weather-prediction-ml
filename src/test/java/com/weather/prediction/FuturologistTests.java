package com.weather.prediction;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.weather.config.TestsConfig;
import com.weather.espatial.Planet;
import com.weather.espatial.PlanetarySystem;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestsConfig.class)
public class FuturologistTests {
	
	final String FERENGI = "Ferengi";
	final String BETASOIDE = "Betasoide";
	final String VULCANO = "Vulcano";

	
	@Autowired
	private Futurologist futurologist;
	
	
	@Test
	public void contextLoads() {
		assertThat(futurologist).isNotNull()
		.isInstanceOf(Futurologist.class);
		assertThat(futurologist.getSystem()).isNotNull()
		.isInstanceOf(PlanetarySystem.class);
		assertThat(futurologist.getSystem().getPlanets().size()).isEqualTo(3);
		futurologist.getSystem().getPlanets().forEach((k,v)->{
			assertThat(v).isNotNull();
			assertThat(v).isInstanceOf(Planet.class);
		});
	}
	
	@Test
	@DirtiesContext
	@Ignore
	public void starInTriangleReturnRainyTest() {
		assertThat(futurologist.predict(0).result).isEqualTo(PredictionResult.RAINY);
		Object[] keys = futurologist.getSystem().getPlanets().keySet().toArray();
		int inc=0;
		for (Object object : keys) {
			futurologist.getSystem().getPlanets().get(object).moveToAngle(75+inc);
			inc+=20;
		}
		assertThat(futurologist.predict(0).result).isEqualTo(PredictionResult.UNDEFINED);
	}
	@Test
	@DirtiesContext
	@Ignore
	public void planetsAllignedReturnOptimalTest() {
		Object[] keys = futurologist.getSystem().getPlanets().keySet().toArray();
		int inc=0;
		for (Object object : keys) {
			futurologist.getSystem().getPlanets().get(object).moveTo(1500f,-200f +inc);
			inc+=500f;
		}
		futurologist.getSystem().getPlanets().forEach((k,v)->assertThat(v.getPosition().getX()).isEqualTo(1500f));
		assertThat(futurologist.predict(0).result).isEqualTo(PredictionResult.PRESSION_TEMPERATURE_OPTIMAL);
	}	
	@Test
	@DirtiesContext
	public void planetsAndStarAllignedReturnDrought() {
		futurologist.getSystem().getPlanets().forEach((k,v)->v.moveToAngle(45f));
		futurologist.getSystem().getPlanets().forEach((k,v)->assertThat(v.getAngle()).isEqualTo(45f));
		assertThat(futurologist.predict(0).result).isEqualTo(PredictionResult.DROUGHT);
		futurologist.getSystem().getPlanets().get(FERENGI).moveToAngle(180f);
		futurologist.getSystem().getPlanets().get(BETASOIDE).moveToAngle(0f);
		futurologist.getSystem().getPlanets().get(VULCANO).moveToAngle(0f);
		assertThat(futurologist.predict(0).result).isEqualTo(PredictionResult.DROUGHT);
	}	
	
	@Test
	@DirtiesContext
	public void perimeterTest() {
		futurologist.getSystem().getPlanets().forEach((k,v)->v.moveToAngle(45f));
		futurologist.getSystem().getPlanets().forEach((k,v)->assertThat(v.getAngle()).isEqualTo(45f));
		assertThat(futurologist.getPerimeter()).isEqualTo(0f);
		futurologist.getSystem().getPlanets().get(FERENGI).moveTo(-200f,500f);
		futurologist.getSystem().getPlanets().get(BETASOIDE).moveTo(400f,300f);
		futurologist.getSystem().getPlanets().get(VULCANO).moveTo(700f,-200f);
		assertThat(futurologist.getPerimeter()).isEqualTo(2355.726f);
	}	
	@Test
	@DirtiesContext
	public void predictNextAlligment() {
		futurologist.getSystem().getPlanets().forEach((k,v)->v.moveToAngle(0f));
		futurologist.getSystem().getPlanets().forEach((k,v)->assertThat(v.getAngle()).isEqualTo(0f));
		assertThat(futurologist.getPerimeter()).isEqualTo(0f);
		assertThat(futurologist.predict(720).result).isEqualTo(PredictionResult.DROUGHT);
	}		


}
