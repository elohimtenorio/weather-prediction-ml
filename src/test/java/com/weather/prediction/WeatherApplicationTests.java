package com.weather.prediction;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.weather.config.TestsConfig;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestsConfig.class)
public class WeatherApplicationTests {

	@Test
	public void contextLoads() {
	}

}
