package com.weather.espatial;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.weather.config.TestsConfig;
import com.weather.espatial.Coordinate;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestsConfig.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class CoordinateTests {
	
	@Autowired
	@Qualifier("coordinate1")
	private Coordinate coordinate;
	
	@Test
	public void whenCreate() {
	    assertThat(coordinate.getX()).isEqualTo(0.0f);
	    assertThat(coordinate.getY()).isEqualTo(0.0f);
	}
	@Test
	public void whenMove() {
		assertThat(coordinate.getX()).isEqualTo(0.0f);
		assertThat(coordinate.getY()).isEqualTo(0.0f);
		coordinate.moveTo(2.1f, -6.5f);
		assertThat(coordinate.getX()).isEqualTo(2.1f);
		assertThat(coordinate.getY()).isEqualTo(-6.5f);
	}
}
