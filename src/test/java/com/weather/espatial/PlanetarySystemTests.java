package com.weather.espatial;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.weather.config.TestsConfig;
import com.weather.espatial.Coordinate;
import com.weather.espatial.Planet;
import com.weather.espatial.PlanetarySystem;
import com.weather.espatial.Star;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestsConfig.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class PlanetarySystemTests {
	
	final String FERENGI = "Ferengi";
	final float DISTANCE_FERENGI = 500f;
	final float SPEED_FERENGI = 1F;
	final boolean CLOCKWISE_FERENGI = true;
	final float ANGLE_FERENGI = 320F;
	
	final String BETASOIDE = "Betasoide";
	final float DISTANCE_BETASOIDE = 2000f;
	final float SPEED_BETASOIDE = 3F;
	final boolean CLOCKWISE_BETASOIDE = true;
	final float ANGLE_BETASOIDE = 180F;
	
	final String VULCANO = "Vulcano";
	final float DISTANCE_VULCANO = 1000f;
	final float SPEED_VULCANO = 5F;
	final boolean CLOCKWISE_VULCANO = false;
	final float ANGLE_VULCANO = 45F;
	
	@Autowired
	@Qualifier("initialPosition")
	private Coordinate initialCoordinate;
	
	@Autowired
	@Qualifier("ferengi")
	private Planet ferengi;	
	
	@Autowired
	@Qualifier("betasoide")
	private Planet betasoide;	
	
	@Autowired
	@Qualifier("vulcano")
	private Planet vulcano;
	
	
	@Autowired
	private PlanetarySystem planetarySystem;
	
	@Test
	public void contextLoads() {
		assertThat(planetarySystem).isNotNull()
		.isInstanceOf(PlanetarySystem.class);
		assertThat(planetarySystem.getStar()).isNotNull()
		.isInstanceOf(Star.class);
		assertThat(planetarySystem.getPlanets().size()).isEqualTo(3);
		planetarySystem.getPlanets().forEach((k,v)->{
			assertThat(v).isNotNull();
			assertThat(v).isInstanceOf(Planet.class);
		});
	}
	
	@Test
	@SuppressWarnings("unlikely-arg-type")
	public void hashPlanetTest() {
		assertThat(ferengi.equals(null)).isEqualTo(false);
		assertThat(ferengi.equals(initialCoordinate)).isEqualTo(false);
		assertThat(planetarySystem.getPlanets().get(FERENGI).equals(ferengi)).isEqualTo(true);
		assertThat(vulcano.equals(ferengi)).isEqualTo(false);
	}
	@Test
	public void planetInfoTest() {
		Planet p = (Planet)planetarySystem.getPlanets().get(FERENGI);
		assertThat(p.getName()).isEqualTo(FERENGI);
		assertThat(p.getAngle()).isEqualTo(ANGLE_FERENGI);
		assertThat(p.getDistance()).isEqualTo(DISTANCE_FERENGI);
		assertThat(p.getSpeed()).isEqualTo(SPEED_FERENGI);
		//TODO test initial move
		assertThat(p.isClockwise()).isEqualTo(true);
	}	
	
	@Test
	public void planetarySystemPosition() {
		assertThat(planetarySystem.getStar().getPosition()).isEqualTo(planetarySystem.getPosition());
		assertThat(planetarySystem.getStar().getPosition().moveTo(30.0f, 40.0f)).isEqualTo(false);
		assertThat(planetarySystem.getStar().getPosition()).isEqualTo(planetarySystem.getPosition());
		assertThat(planetarySystem.getStar().getPosition().getX()).isNotEqualTo(30.0f);
		assertThat(planetarySystem.getStar().getPosition().getY()).isNotEqualTo(40.0f);
		assertThat(planetarySystem.getStar().getPosition().getX()).isEqualTo(0.0f);
		assertThat(planetarySystem.getStar().getPosition().getY()).isEqualTo(0.0f);
	}
	@Test 
	public void movePlanetsClockwise() {
		assertThat(betasoide.getPosition()).isEqualTo(new Coordinate(-2000.0f,0.0f));
		planetarySystem.moveDays(30);
		assertThat(betasoide.getPosition()).isNotEqualTo(new Coordinate(-2000.0f,0.0f));
		assertThat(betasoide.getPosition()).isEqualTo(new Coordinate(0.0f,2000.0f));
	}
	@Test 
	public void movePlanetsAntiClockwise() {
		assertThat(vulcano.getPosition()).isEqualTo(new Coordinate(707.11f,707.11f));
		planetarySystem.moveDays(30);
		assertThat(vulcano.getPosition()).isNotEqualTo(new Coordinate(707.11f,707.11f));
		assertThat(vulcano.getPosition()).isEqualTo(new Coordinate(-965.93f,-258.82f));
	}	

}
