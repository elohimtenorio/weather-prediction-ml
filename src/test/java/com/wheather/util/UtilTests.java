package com.wheather.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.weather.config.TestsConfig;
import com.weather.espatial.Coordinate;
import com.weather.util.Util;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestsConfig.class)
public class UtilTests {
	
	@Autowired
	@Qualifier("coordinate1")
	private Coordinate coordinate1;
	
	@Autowired
	@Qualifier("coordinate2")
	private Coordinate coordinate2;
	
	@Autowired
	@Qualifier("coordinate3")
	private Coordinate coordinate3;

	@Autowired
	@Qualifier("coordinateNegative")
	private Coordinate coordinateNegative;
	
	
	final float ANGLE_45 = 45f;
	final float ANGLE_90 = 90f;
	
	@Test
	public void contextLoads() {
		assertThat(new Util()).isNotNull();
		assertThat(coordinate1).isNotNull();
		assertThat(coordinate1.getX()).isEqualTo(0.0f);
		assertThat(coordinate1.getY()).isEqualTo(0.0f);
		assertThat(coordinate2).isNotNull();
		assertThat(coordinate2.getX()).isEqualTo(2.0f);
		assertThat(coordinate2.getY()).isEqualTo(2.0f);
		assertThat(coordinate3).isNotNull();
		assertThat(coordinate3.getX()).isEqualTo(2.0f);
		assertThat(coordinate3.getY()).isEqualTo(0.0f);
	}
	
	@Test
	public void gpd2rpdTest() {
		assertThat(Util.gpd2rpd(90)).isEqualTo(0.25);
		assertThat(Util.gpd2rpd(180)).isEqualTo(0.5);
		assertThat(Util.gpd2rpd(270)).isEqualTo(0.75);
		assertThat(Util.gpd2rpd(360)).isEqualTo(1.0);
	}
	
	@Test
	public void angleBetweenTest() {
		assertThat(Util.angleBetween(coordinate1, coordinate2, coordinate3))
		.isNotNegative()
		.isEqualTo(ANGLE_45);
		assertThat(Util.angleBetween(coordinate2, coordinate1, coordinate3))
		.isNotNull()
		.isNotNegative()
		.isEqualTo(ANGLE_45);
		assertThat(Util.angleBetween(coordinate3, coordinate1, coordinate2))
		.isNotNull()
		.isNotNegative()
		.isEqualTo(ANGLE_90);
	}
	
	@Test
	public void getAngleTest() {
		assertThat(Util.getAngle( coordinate2))
		.isNotNegative()
		.isEqualTo(ANGLE_45);
	}
	
	@Test
	public void whenNegativeAngleTest() {
		assertThat(Util.getAngle(coordinateNegative))
		.isNotNull()
		.isNotNegative()
		.isEqualTo(ANGLE_45);
	}
	
	@Test
	public void angleNaturalBetweenTest() {
		assertThat(Util.angleNaturalBetween(coordinate1, coordinate2, coordinate3))
		.isNotNegative()
		.isEqualTo(ANGLE_45);
		assertThat(Util.angleNaturalBetween(coordinate2, coordinate1, coordinate3))
		.isNotNull()
		.isNotNegative()
		.isEqualTo(315f);
		assertThat(Util.angleNaturalBetween(coordinate3, coordinate1, coordinate2))
		.isNotNull()
		.isNotNegative()
		.isEqualTo(ANGLE_90);
		assertThat(Util.getAngleNatural(new Coordinate(0f, -5f))).isEqualTo(270f);
		assertThat(Util.angleNaturalBetween(new Coordinate(0f, 0f), new Coordinate(-5f, 0f), new Coordinate(0f, -5f))).isEqualTo(270f);
	}
	

}
