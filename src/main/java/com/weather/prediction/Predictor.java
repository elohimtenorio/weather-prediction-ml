package com.weather.prediction;

import com.weather.repository.Prediction;

public interface Predictor {
	
	public Prediction predict(int day);
	
	

}
