package com.weather.prediction;

import java.util.Map;

import com.weather.espatial.AstronomicalObject;
import com.weather.espatial.Planet;
import com.weather.util.Util;

public class AngleCalculator {
	private Map<String,Planet> previous;
	private Map<String,Planet> actuals;
	//TODO add star
	
	
	public AngleCalculator(Map<String, Planet> previous, Map<String, Planet> actuals) {
		this.previous = previous;
		this.actuals = actuals;
	}


	public void setPrevious(Map<String, Planet> previous) {
		this.previous = previous;
	}


	public void setActuals(Map<String, Planet> actuals) {
		this.actuals = actuals;
	}



	public boolean wasAlligned() {
		Object[] keysPrevious = previous.keySet().toArray();
		Object[] keyActuals = actuals.keySet().toArray();
		float pa1 =Util.angleNaturalBetween(((AstronomicalObject)previous.get(keysPrevious[0])).getPosition()
				,((AstronomicalObject)previous.get(keysPrevious[1])).getPosition()
				,((AstronomicalObject)previous.get(keysPrevious[2])).getPosition());
		float pa2 =Util.angleNaturalBetween(((AstronomicalObject)previous.get(keysPrevious[1])).getPosition()
				,((AstronomicalObject)previous.get(keysPrevious[0])).getPosition()
				,((AstronomicalObject)previous.get(keysPrevious[2])).getPosition());
		float pa3 =Util.angleNaturalBetween(((AstronomicalObject)previous.get(keysPrevious[2])).getPosition()
				,((AstronomicalObject)previous.get(keysPrevious[0])).getPosition()
				,((AstronomicalObject)previous.get(keysPrevious[1])).getPosition());
		
		
		float aa1 =Util.angleNaturalBetween(((AstronomicalObject)actuals.get(keyActuals[0])).getPosition()
				,((AstronomicalObject)actuals.get(keyActuals[1])).getPosition()
				,((AstronomicalObject)actuals.get(keyActuals[2])).getPosition());
		float aa2 =Util.angleNaturalBetween(((AstronomicalObject)actuals.get(keyActuals[1])).getPosition()
				,((AstronomicalObject)actuals.get(keyActuals[0])).getPosition()
				,((AstronomicalObject)actuals.get(keyActuals[2])).getPosition());
		float aa3 =Util.angleNaturalBetween(((AstronomicalObject)actuals.get(keyActuals[2])).getPosition()
				,((AstronomicalObject)actuals.get(keyActuals[0])).getPosition()
				,((AstronomicalObject)actuals.get(keyActuals[1])).getPosition());
		
		if (pa1==180f || aa1 == 180f || aa1 == 0f || aa1 == 0f)
			return true;
		
		return(
				((pa1>180f && aa1>180f)||(pa1<180f && aa1<180f)
						&& (pa2>180f && aa2>180f)||(pa2<180f && aa2<180f)
						&& (pa3>180f && aa3>180f)||(pa3<180f && aa3<180f))
				);
		
		
	};
	
	
	
	
	
	
}
