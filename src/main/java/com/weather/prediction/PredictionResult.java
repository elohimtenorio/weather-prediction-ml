package com.weather.prediction;

public class PredictionResult {
	public static final String DROUGHT = "Sequia";
	public static final String RAINY = "Lluvia";
	public static final String UNDEFINED = "sin definir";
	public static final String PRESSION_TEMPERATURE_OPTIMAL = "presion optima";
}
