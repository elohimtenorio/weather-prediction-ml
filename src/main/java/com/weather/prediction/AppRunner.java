package com.weather.prediction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import com.weather.repository.Prediction;

import com.weather.repository.PredictionRepository;


//@Component
public class AppRunner implements CommandLineRunner{
	
	private Futurologist futurologist;
	private int day;
	private final int MAX_DAYS=10000;
	@Autowired
	private PredictionRepository repository;

	public AppRunner() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run(String... args) throws Exception {
		setDay(0);
		repository.deleteAll();
		while(getDay()<MAX_DAYS) {
			Prediction prediction = futurologist.predict(getDay());
			repository.save(prediction);
		}

		
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}
	
	

}
