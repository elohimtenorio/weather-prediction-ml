package com.weather.prediction;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weather.espatial.Planet;
import com.weather.espatial.PlanetarySystem;
import com.weather.repository.PlanetModel;
import com.weather.repository.Prediction;
import com.weather.util.Util;

@Service
public class Futurologist{
	
	@Autowired
	private PlanetarySystem planetarySystem;

	public PlanetarySystem getSystem() {
		return planetarySystem;
	}
	public void setSystem(PlanetarySystem system) {
		this.planetarySystem = system;
	}

	public  Prediction predict(int day) {
		if (getDay()!=day)
			planetarySystem.moveDays(day);
		String predictionResult;
		if (planetsAlligned()) {
			if (starAlligned())
				predictionResult=PredictionResult.DROUGHT;
			else
				predictionResult = PredictionResult.PRESSION_TEMPERATURE_OPTIMAL;
		}else
		if (starInTriangle()) {
			predictionResult = PredictionResult.RAINY;
		}else
			predictionResult = PredictionResult.UNDEFINED;
		List <PlanetModel> planetsModel = new LinkedList<>();
		planetarySystem.getPlanets().values().forEach((e)->
			planetsModel.add(new PlanetModel(e.getName(), e.getDistance(), e.getSpeed(), e.getAngle(),
					e.getPosition().getX(), e.getPosition().getY())));
		return (new Prediction(getDay(), predictionResult, getPerimeter(),planetsModel));
		
	}
	
	public boolean planetsAlligned() {
		Map<String,Planet> planets = planetarySystem.getPlanets();
		Object[] keys = planets.keySet().toArray();
		float angle = Util.angleBetween(planets.get(keys[0]).getPosition(),planets.get(keys[1]).getPosition(),
				planets.get(keys[2]).getPosition());
		return (angle==0f || angle == 180f);
	}
	
	public boolean starAlligned() {
		if (!planetsAlligned())
			return false;
		Map<String,Planet> planets = planetarySystem.getPlanets();
		Object[] keys = planets.keySet().toArray();
		float angle = Util.angleBetween(planetarySystem.getStar().getPosition(),planets.get(keys[0]).getPosition(),planets.get(keys[1]).getPosition());
		return (angle==0f || angle == 180f);
	}
	
	public boolean starInTriangle() {
		Map<String,Planet> planets = planetarySystem.getPlanets();
		Object[] keys = planets.keySet().toArray();
		return Util.angleBetween(
				planets.get(keys[0]).getPosition(),(planets.get(keys[1])).getPosition(),planets.get(keys[2]).getPosition()) 
				> Util.angleBetween(planets.get(keys[0]).getPosition(),planets.get(keys[1]).getPosition()
						,planetarySystem.getPosition())
				&& Util.angleBetween(
						planets.get(keys[1]).getPosition(),planets.get(keys[0]).getPosition(),planets.get(keys[2]).getPosition()) 
					> Util.angleBetween(planets.get(keys[1]).getPosition(),planets.get(keys[0]).getPosition()
						,planetarySystem.getPosition());
	}	
	
	public float getPerimeter() {
		if (planetsAlligned())
				return 0f;
		Map<String,Planet> planets = planetarySystem.getPlanets();
		Object[] keys = planets.keySet().toArray();
		return planets.get(keys[0]).getDistanceTo(planets.get(keys[1]))+planets.get(keys[0]).getDistanceTo(planets.get(keys[2]))
				+planets.get(keys[1]).getDistanceTo(planets.get(keys[2]));
		
	}
	public int getDay() {
		return this.planetarySystem.getDay();
	}

	
	

}
