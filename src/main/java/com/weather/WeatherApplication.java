package com.weather;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.weather.prediction.Futurologist;
import com.weather.prediction.PredictionResult;
import com.weather.repository.Prediction;
import com.weather.repository.PredictionRepository;


@SpringBootApplication
@ComponentScan
public class WeatherApplication implements CommandLineRunner{
	
	private static final Logger log = LoggerFactory.getLogger(WeatherApplication.class);
	
	@Autowired
	private PredictionRepository repository;
	@Autowired
	private Futurologist futurologist;
	@Autowired
	private int day;
	private final int MAX_DAYS=365*10;
	public static void main(String[] args) {
		SpringApplication.run(WeatherApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		setDay(0);
		repository.deleteAll();
		log.info(String.format("Iniciando analisis para los proximos %s dias",MAX_DAYS));//+prediction);
		List<Prediction> listPredictions = new LinkedList<>();
		int saved = 0;
		while(getDay()<MAX_DAYS) {
			listPredictions.add(futurologist.predict(getDay()));
			//log.info("Saving prediction at day "+getDay());//+prediction);
			setDay(getDay()+1);
			if (listPredictions.size()==1000) {
				repository.saveAll(listPredictions);
				saved += listPredictions.size();
				listPredictions.clear();
			}
			
		}
		repository.saveAll(listPredictions);
		saved += listPredictions.size();
		log.info("Saved " + repository.count() + "Records");
		
		int rainyDays = repository.findByResult(PredictionResult.RAINY).size();
		int droughtDays = repository.findByResult(PredictionResult.DROUGHT).size();
		int optimalDays = repository.findByResult(PredictionResult.PRESSION_TEMPERATURE_OPTIMAL).size();
		int undefinedDays = repository.findByResult(PredictionResult.UNDEFINED).size();
		
		log.info("Dias de lluvia " + rainyDays);
		log.info("Dias de sequia " + droughtDays);
		log.info("Dias de presion y temperatura optima " + optimalDays);
		log.info("Dias sin condiciones para predecir " + undefinedDays);

		
	}
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

}
