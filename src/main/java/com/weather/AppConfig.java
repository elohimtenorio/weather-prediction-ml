package com.weather;

import java.util.Map;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.weather.espatial.AstronomicalObject;
import com.weather.espatial.Coordinate;
import com.weather.espatial.Planet;
import com.weather.espatial.PlanetarySystem;
import com.weather.espatial.Star;
import com.weather.prediction.Futurologist;

@Configuration
public class AppConfig {
	
	final String FERENGI = "Ferengi";
	final float DISTANCE_FERENGI = 500f;
	final float SPEED_FERENGI = 1F;
	final boolean CLOCKWISE_FERENGI = true;
	final float ANGLE_FERENGI = 0F;
	
	final String BETASOIDE = "Betasoide";
	final float DISTANCE_BETASOIDE = 2000f;
	final float SPEED_BETASOIDE = 3F;
	final boolean CLOCKWISE_BETASOIDE = true;
	final float ANGLE_BETASOIDE = 180F;
	
	final String VULCANO = "Vulcano";
	final float DISTANCE_VULCANO = 1000f;
	final float SPEED_VULCANO = 5F;
	final boolean CLOCKWISE_VULCANO = false;
	final float ANGLE_VULCANO = 0F;
	

	@Bean
	@Qualifier("initialPosition")
	public Coordinate getInitialCoordinate() {
	    return new Coordinate(0.0f,0.0f);
	}
	@Bean
	@Qualifier("coordinate1")
	public Coordinate getCoordinate1() {
	    return new Coordinate(0.0f,0.0f);
	}
	@Bean
	@Qualifier("coordinate2")
	public Coordinate getCoordinate2() {
	    return new Coordinate(2.0f,2.0f);
	}
	@Bean
	@Qualifier("coordinate3")
	public Coordinate getCoordinate3() {
	    return new Coordinate(2.0f,0.0f);
	}
	@Bean
	@Qualifier("coordinateNegative")
	public Coordinate getCoordinateNegative() {
	    return new Coordinate(2.0f,-2.0f);
	}
	
	@Bean
	@Qualifier("star")
	public AstronomicalObject getStar() {
		return new Star();
	}
	
	@Bean
	@Qualifier("ferengi")
	public Planet getFerengi() {
		return new Planet(FERENGI,DISTANCE_FERENGI,SPEED_FERENGI,CLOCKWISE_FERENGI,ANGLE_FERENGI); 
	}
	
	@Bean
	@Qualifier("betasoide")
	public Planet getBetasoide() {
		return new Planet(BETASOIDE,DISTANCE_BETASOIDE,SPEED_BETASOIDE,CLOCKWISE_BETASOIDE,ANGLE_BETASOIDE);
	}
	
	@Bean
	@Qualifier("vulcano")
	public Planet getVulcano() {
		return new Planet(VULCANO,DISTANCE_VULCANO,SPEED_VULCANO,CLOCKWISE_VULCANO,ANGLE_VULCANO);
	}	
	
	@Bean 
	@Qualifier("planetsMap")
	public Map<String,Planet> getPlanets(){
		Map<String, Planet> planetMap = new TreeMap<>();
		planetMap.put(getFerengi().getName(),getFerengi());
		planetMap.put(getBetasoide().getName(),getBetasoide());
		planetMap.put(getVulcano().getName(),getVulcano());
		return planetMap;
	}
	
	@Bean
	public PlanetarySystem getSystem() {
		return new PlanetarySystem();
	}
	
	@Bean
	@Qualifier("speedFerengi")
	public float getSpeedFerengi() {
		return SPEED_FERENGI;	
	}
	
	@Bean
	@Qualifier("speedBetasoide")
	public float getSpeedBetasoide() {
		return SPEED_BETASOIDE;	
	}
	
	@Bean
	@Qualifier("speedVulcano")
	public float getSpeedVulcano() {
		return SPEED_VULCANO;	
	}
	
	@Bean 
	@Qualifier("day")
	public int getDay() {
		return 0;
	}
	
	@Bean
	public Futurologist getFuturologist() {
		return new Futurologist();
	}

}
