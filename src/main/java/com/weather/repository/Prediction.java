package com.weather.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Prediction {
	
	@Id
	public ObjectId Id;
	public int day;
	public String result;
	public float perimeter;
	
	public List<PlanetModel> planets;
	public Prediction (int day, String result, float perimeter ,List<PlanetModel> planets) {
		this.day = day;
		this.result = result;
		this.perimeter = perimeter;
		this.planets = planets;
	
		
	}
	@Override
	public String toString() {
		return "Prediction [Id=" + Id + ", day=" + day + ", result=" + result + ", perimeter=" + perimeter
				+ ", planets=" + planets + "]";
	}

	
	
	

}
