package com.weather.repository;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;


public class PlanetModel {
	@Id
	public ObjectId id;
	public String name;
	public float distance;
	public float speed;
	public float angle;
	public float x;
	public float y;
	public PlanetModel(String name, float distance, float speed, float angle, float x, float y) {
		this.name = name;
		this.distance = distance;
		this.speed = speed;
		this.angle = angle;
		this.x = x;
		this.y = y;
	}
	@Override
	public String toString() {
		return "PlanetModel [id=" + id + ", name=" + name + ", distance=" + distance + ", speed=" + speed + ", angle="
				+ angle + ", x=" + x + ", y=" + y + "]";
	}

	
	
	
	
	
	
	
	

}
