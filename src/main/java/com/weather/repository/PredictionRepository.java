package com.weather.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "clima", path = "clima")
public interface PredictionRepository extends MongoRepository<Prediction, String> {

	public Prediction findByDay(@Param("dia") int day);
	public List<Prediction> findByResult(@Param("resultado")String result);
	
}
