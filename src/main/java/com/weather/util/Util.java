package com.weather.util;

import com.weather.espatial.Coordinate;

public final class Util {
	
	public static double gpd2rpd(double gpd) {
		return gpd/360;
	}
	public static float getAngle(Coordinate p) {
		return Util.angleBetween(new Coordinate(0,0), p, new Coordinate(1,0));
	}
	public static float angleBetween(Coordinate p1, Coordinate p2, Coordinate p3) {
		double ang1 = Math.atan2((p2.getX()-p1.getX()),(p2.getY()-p1.getY()));
		double ang2 = Math.atan2((p3.getX()-p1.getX()),(p3.getY()-p1.getY()));
		double ang = ang2-ang1;
		ang = ang<0f?ang+(2.0*Math.PI):ang;
		return (float)Math.toDegrees(ang > Math.PI?2.0*Math.PI-ang:ang);
	}
	public static float angleNaturalBetween(Coordinate p1, Coordinate p2,
			Coordinate p3) {
		// TODO Auto-generated method stub
		double ang1 = Math.atan2((p2.getX()-p1.getX()),(p2.getY()-p1.getY()));
		double ang2 = Math.atan2((p3.getX()-p1.getX()),(p3.getY()-p1.getY()));
		double ang = ang2-ang1;
		return (float) Math.toDegrees( ang<0f?ang+(2.0*Math.PI):ang);
	}
	public static float getAngleNatural(Coordinate p) {
		// TODO Auto-generated method stub
		return Util.angleNaturalBetween(new Coordinate(0,0), p, new Coordinate(1,0));
	}

}
