package com.weather.espatial;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
@Service
public class PlanetarySystem implements AstronomicalObject {
	
	@Autowired
	@Qualifier("star")
	private AstronomicalObject star;
	@Autowired
	@Qualifier("planetsMap")
	private Map<String,Planet> planets;
	private int day;
	
	
	@Override
	public Coordinate getPosition() {
		return this.getStar().getPosition();
	}

	@Override
	public boolean moveTo(float x, float y) {
		return false;
	}


	public AstronomicalObject getStar() {
		return star;
	}

	public void setStar(AstronomicalObject star) {
		this.star = star;
	}

	public Map<String,Planet> getPlanets() {
		return planets;
	}

	public void addPlanet(Planet p) {
		this.planets.put(p.getName(),p);
	}
	
	private void moveDays(Planet p, int days) {
		float angleMoved = days*p.getSpeed();
		float newAngle = p.isClockwise()?p.getAngle()-angleMoved:p.getAngle()+angleMoved;
		p.moveToAngle((int)(newAngle<0f?newAngle+360:(newAngle>=360?newAngle-360:newAngle)));
	}
	@Autowired
	@Qualifier("day")
	public void moveDays(int days) {
		this.day = days;
		getPlanets().forEach((k,v)->moveDays((Planet)v,days));
	}

	public int getDay() {
		return day;
	}
	
	

}
