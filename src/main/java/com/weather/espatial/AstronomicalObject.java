package com.weather.espatial;

public interface AstronomicalObject {
	
	public Coordinate getPosition();
	public boolean moveTo(float x, float y);

	
	
	

}
