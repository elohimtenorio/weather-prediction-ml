package com.weather.espatial;

public class Star implements AstronomicalObject {
	
	private Coordinate position;
	

	public Star() {
		this.position = new MainStarCoordinate();
	}
	
	@Override
	public Coordinate getPosition() {
		return position;
	}

	@Override
	public boolean moveTo(float x, float y) {
		return false;
	}

}
