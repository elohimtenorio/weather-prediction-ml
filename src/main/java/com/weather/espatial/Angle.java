package com.weather.espatial;

public class Angle {
	private String p0;
	private String p1;
	private String p2;
	private float angle;
	public String getP0() {
		return p0;
	}
	public void setP0(String p0) {
		this.p0 = p0;
	}
	public String getP1() {
		return p1;
	}
	public void setP1(String p1) {
		this.p1 = p1;
	}
	public String getP2() {
		return p2;
	}
	public void setP2(String p2) {
		this.p2 = p2;
	}
	public float getAngle() {
		return angle;
	}
	public void setAngle(float angle) {
		this.angle = angle;
	}
	
}
