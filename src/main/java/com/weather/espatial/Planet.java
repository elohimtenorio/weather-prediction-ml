package com.weather.espatial;


public class Planet implements AstronomicalObject, Comparable<Planet>{
	
	private String name;
	private Coordinate position;
	private float angle;
	private float initialAngle;
	private float distance;
	private float speed;
	private boolean clockwise;
	
	public Planet(String name, float distance, float speed, boolean clockwise, float initialAngle) {
		this.setName(name);
		this.setDistance(distance);
		this.setSpeed(speed);
		this.setClockwise(clockwise);
		this.position = new Coordinate();
		this.setInitialAngle(initialAngle);
		this.moveToAngle(initialAngle);
		
	}

	@Override
	public Coordinate getPosition() {
		return position;
	}

	@Override
	public boolean moveTo(float x, float y) {
		return getPosition().moveTo(x, y);
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public boolean isClockwise() {
		return clockwise;
	}

	public void setClockwise(boolean clockwise) {
		this.clockwise = clockwise;
	}

	public float getAngle() {
		return this.angle;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public void moveToAngle(float angle) {
		this.angle = angle;
		float newX = (float) Math.round((getDistance() * Math.cos(Math.toRadians(angle)))*100)/100;
		float newY =  (float) Math.round((getDistance() * Math.sin(Math.toRadians(angle)))*100)/100;
		moveTo(newX, newY);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Planet other = (Planet) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	@Override
	public int compareTo(Planet o) {
		return (int) (distance - o.getDistance());
	}
	
	public float getDistanceTo(AstronomicalObject obj){
		return (float) Math.sqrt(Math.pow((position.getX()-obj.getPosition().getX()),2)
				+ Math.pow((position.getY()-obj.getPosition().getY()),2));
		
	}

	public float getInitialAngle() {
		return initialAngle;
	}

	public void setInitialAngle(float initialAngle) {
		this.initialAngle = initialAngle;
	}
	

}
