package com.weather.espatial;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Coordinate {
	//@NotNull(message = "X may not be null")
	private float x;
	//@NotNull(message = "Y may not be null")
	private float y;
	
	public Coordinate() {
		this.x = this.y = 0.0f;
	}
	
	public Coordinate(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean moveTo(float x, float y) {
		this.x = x;
		this.y = y;
		return (getX() == x && getY() == y);

	}
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinate other = (Coordinate) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
			return false;
		return true;
	}




	
	

	
	

}
